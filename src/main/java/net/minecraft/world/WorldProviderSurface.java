package net.minecraft.world;

public class WorldProviderSurface extends WorldProvider {
	private static final String __OBFID = "CL_00000388";

	@Override
	public String getDimensionName() {
		return "Overworld";
	}
}