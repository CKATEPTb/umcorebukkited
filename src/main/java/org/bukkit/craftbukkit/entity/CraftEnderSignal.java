package org.bukkit.craftbukkit.entity;

import org.bukkit.craftbukkit.CraftServer;
import org.bukkit.entity.EnderSignal;
import org.bukkit.entity.EntityType;

import net.minecraft.entity.item.EntityEnderEye;

public class CraftEnderSignal extends CraftEntity implements EnderSignal {
	public CraftEnderSignal(CraftServer server, EntityEnderEye entity) {
		super(server, entity);
	}

	@Override
	public EntityEnderEye getHandle() {
		return (EntityEnderEye) entity;
	}

	@Override
	public String toString() {
		return "CraftEnderSignal";
	}

	@Override
	public EntityType getType() {
		return EntityType.ENDER_SIGNAL;
	}
}